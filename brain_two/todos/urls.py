from django.urls import path
from todos.views import create_list_view, show_list_detail

urlpatterns = [
    path("", create_list_view, name="todo_list_list"),
    path("<int:id>/", show_list_detail, name="todo_list_detail"),
]
