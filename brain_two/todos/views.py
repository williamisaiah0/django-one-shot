from django.shortcuts import render, get_object_or_404
from todos.models import TodoList

# Create your views here.


def create_list_view(request):
    lists = TodoList.objects.all()
    context = {
        "all_lists": lists,
    }
    return render(request, "list.html", context)


def show_list_detail(request):
    tasks = get_object_or_404(TodoList, id=id)
    context = {
        "tasks": tasks,
    }
    return render(request, "details.html", context)
